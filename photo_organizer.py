#! /usr/bin/env python2
""" Photo Organizer - 6/16/2012

"""

import os
import glob
from PIL import Image
from PIL.ExifTags import TAGS
from optparse import OptionParser

def all_cases(file_type):
    """ This function takes in a list of file extensions and returns
        that list, but appends the lower or upper case version of the 
        members
    """
    for entry_num in range(len(file_type)):
        if file_type[entry_num].isupper() == True:
            file_type.append(str(file_type[entry_num]).lower())
        if file_type[entry_num].islower() == True:
            file_type.append(str(file_type[entry_num]).upper())
            
            
def sort(options):
    """ This function moves files of a certain extension and moves them
        from a parent directory into subdirectories. 
    """
    photos = glob.glob('*.*' + str(options.file_type))
    for num_file in range(len(photos)):
        tags = {}
        photo_file = Image.open(photos[num_file])
        info = photo_file._getexif()
#        photo_file.close()
        if info:
            for tag, value in info.items():
                decoded = TAGS.get(tag, tag)
                tags[decoded] = value
#                print tag, value
        if tags == {}: # No tags
            new_dir = "No_EXIF"
        else: # Has tags
            #date = str(tags['EXIF DateTimeOriginal'])
            date = str(tags['DateTime'])
#            print date
            year = date[0:4]
            month = date[5:7]
            day = date[8:10]
            if options.folders == "wide":
                new_dir = str(year) + '_' + str(month) + '_' + str(day)
            elif options.folders == "deep":
                new_dir = os.path.join(str(year), str(month), str(day))
            else:
                print "Error"
        
        move_picture(new_dir, photos[num_file], options.verbose)
        
        del tags


def move_picture(new_dir, photo, verbose):
    """ This function moves a picture into a specified folder. 
    """
    # See if the path exists already, if not, make it
    if os.path.isdir(new_dir) == False:
        os.makedirs(new_dir)
       
    os.rename(photo, os.path.join(new_dir, photo))
    if verbose == True:          
        print "Sorting file: ", photo, " to folder", new_dir, "/"

            
def unsort(options):
    """ This function moves files of a certain extension and moves them
        from subdirectories into the parent directory. 
    """
    for root, dirs, files in os.walk(os.curdir):
        for filetype in options.file_type:
            for filename in glob.glob(root + "/*" + filetype):
                os.rename(filename, 
                          os.path.join(os.getcwd(), 
                          os.path.basename(filename)))
                if options.verbose == True:
                    print 'Unsorting file: ', os.path.basename(filename)


def main():
    parser = OptionParser()
    parser.add_option("-t", "--type", action="append", dest="file_type",
        default=[], help="type of file extension to look for")
    parser.add_option("-c", "--case", dest="case", 
        action="store_true", default=False, help="respects case of input")
    parser.add_option("-q", "--quiet", dest="verbose",
        action="store_false", default=True, 
        help="quiet the status messages")
    parser.add_option("-f", "--folders", action="store", dest="folders",
        default="wide", help="wide or deep folder structure")
    parser.add_option("-u", "--unsort", dest="unsort", 
        action="store_true", default=False, 
        help="removes files from folders and puts them in a common directory")
        
    (options, args) = parser.parse_args()
    
    # print("options:", options)
    # print("arguments:", args)

    # Default file types
    if options.file_type == []:
        default_formats = [".jpg", ".tif", ".raw"]
        options.file_type.extend(default_formats)

    # Case of file types
    if options.case == False:
        all_cases(options.file_type)
    
    # Verbose output of file types
    if options.verbose == True:
        print "Sorting types: ", str(options.file_type)
    
    # File sorting
    if options.unsort == False:
        sort(options)
        
    # File unsorting
    if options.unsort == True:
        unsort(options)


if __name__ == '__main__':
    main()
